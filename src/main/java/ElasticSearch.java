import JsonHelper.MapperUtil;
import com.fasterxml.jackson.databind.JsonNode;
import config.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.search.MatchQuery;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.*;

public class ElasticSearch {


    static RestClient restClient = null;
    static RestHighLevelClient restHighLevelClient = null;
    static SearchRequest searchRequest;

    static {
        try {
            searchRequest = new SearchRequest(Config.ELASTICSEARCH_LOCATION_INDEX);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    public static void establishConnection() {
        String host = Config.ELASTICSEARCH_HOST;
        List<String> hosts = new ArrayList<>(Arrays.asList(host.split(",")));
        restClient = RestClient.builder(new HttpHost(hosts.get(0), 9200, "http")).build();
        restHighLevelClient = new RestHighLevelClient(restClient);
        return;
    }

    public static void stopConnection() throws IOException {
        if (restClient != null) restClient.close();
    }

    public static List<JsonNode> mustKeywordSearch(String city, String state, String country,
                                                   String stateCode, String countryCode) throws IOException {
        Map<String, String> ans = new HashMap<>();
        ans.put("city", WordUtils.capitalize(city));
        ans.put("state", WordUtils.capitalize(state));
        ans.put("country", WordUtils.capitalize(country));
        ans.put("state_code", stateCode.toUpperCase());
        ans.put("country_code", countryCode.toUpperCase());
        Set<String> set = ans.keySet();
        Iterator<String> i = set.iterator();
        BoolQueryBuilder qb = QueryBuilders.boolQuery();
        while (i.hasNext()) {
            String field = i.next();
            String l = ans.get(field);
            if (StringUtils.isNotBlank(l)) {
                qb.must(QueryBuilders.matchQuery(field + ".keyword", l));
            }
        }
        searchSourceBuilder.query(qb);
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest);
        //      float max = searchResponse.getHits().getMaxScore();
        SearchHit[] hits = searchResponse.getHits().getHits();
        List<JsonNode> values = new ArrayList<>();
        for (SearchHit hit : hits) {
            if (values.size() < 6) {
                values.add(MapperUtil.fromMap(hit.getSource(), JsonNode.class));
            } else break;
        }
        return values;
    }

}
