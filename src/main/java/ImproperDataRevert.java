import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.ReadConfig;
import com.mongodb.spark.rdd.api.java.JavaMongoRDD;
import config.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

import java.util.*;

public class ImproperDataRevert {
    public static void main(String[] args) throws IOException, ParseException {
        //Create a SparkContext to initialize
        ElasticSearch.establishConnection();
        Properties prop = new Properties();
        prop.setProperty("bootstrap.servers", Config.KAFKA_BROKER_LIST);
        prop.setProperty("key.serializer", Config.KAFKA_KEY_SERIALIZER);
        prop.setProperty("value.serializer", Config.KAFKA_VALUE_SERIALIZER);
        prop.setProperty("crmoutputTopic", Config.PRODUCE_KAFKA_TOPIC_NAME);
        Set<String> stateCodes = new HashSet<>();

        String[] stateCode = Config.STATE_CODES.split(",");
        for (String n : stateCode) stateCodes.add(n);

        List<String> ClientList = Arrays.asList(Config.REFNUMLIST.split(","));
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("readfromMongo")
                .set("spark.mongodb.input.uri", Config.MONGODB_URI)
                .set("spark.mongodb.output.uri", Config.MONGODB_URI)
                .set("spark.mongodb.output.database", "");

        conf.set("spark.mongodb.input.database", "");
        conf.set("spark.mongodb.input.collection", "profileEvent");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Create a Java version of the Spark Context
        Set<String> Unstandardvalues = new HashSet<>();
        for (String client : ClientList) {

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.append("status", "standard");
            basicDBObject.append("countryCode", new BasicDBObject("$exists", true));
            basicDBObject.append("Standardvalue.country", new BasicDBObject("$exists", true));
            basicDBObject.append("Standardvalue.city", new BasicDBObject("$exists", false));
            basicDBObject.append("Standardvalue.state", new BasicDBObject("$exists", false));

            DB db = MongoDB.readData(client.trim());
            DBCollection collection = db.getCollection("nonStandardLocation");
            DBCursor cursor = collection.find(basicDBObject);

            while (cursor.hasNext()) {
                DBObject dbObject = cursor.next();
                Object unstandardvalue = dbObject.get("Unstandardvalue");
                Object countryCode = dbObject.get("countryCode");
                Object standardvalue = dbObject.get("Standardvalue");
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(standardvalue.toString());
                String location = json.get("location").toString();
                if (unstandardvalue != null &&
                        !(unstandardvalue.toString().trim().equalsIgnoreCase(location.trim())) &&
                stateCodes.contains(countryCode.toString().toUpperCase())) {
                        Unstandardvalues.add(unstandardvalue.toString());
                }
            }
            basicDBObject.clear();
            basicDBObject.append("Standardvalue.country", new BasicDBObject("$exists", true));
            basicDBObject.append("Standardvalue.city", new BasicDBObject("$exists", true));
            basicDBObject.append("Standardvalue.state", new BasicDBObject("$exists", false));
            basicDBObject.append("status", "standard");
            basicDBObject.append("countryCode", new BasicDBObject("$exists", true));
            basicDBObject.append("stateCode", new BasicDBObject("$exists", false));

            cursor = collection.find(basicDBObject);
            while (cursor.hasNext()) {
                DBObject dbObject = cursor.next();
                Object unstandardvalue = dbObject.get("Unstandardvalue");
                Object countryCode = dbObject.get("countryCode");
                Object standardvalue = dbObject.get("Standardvalue");
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(standardvalue.toString());
                String location = json.get("location").toString();
                String city = json.get("city").toString();
                String country = json.get("country").toString();

               int count =  ElasticSearch.mustKeywordSearch(city,"","",countryCode.toString(),"").size();
               if (count > 0 & unstandardvalue != null &&
                        !(unstandardvalue.toString().trim().equalsIgnoreCase(location.trim())) &&
                        stateCodes.contains(countryCode.toString().toUpperCase())) {
                    Unstandardvalues.add(unstandardvalue.toString());
                }
            }

            Broadcast<Set<String>> broadcast = sc.broadcast(Unstandardvalues);
            Broadcast<String> broadcast_client = sc.broadcast(client);
            Broadcast<Properties> propertiesBroadcast = sc.broadcast(prop);
            Map<String,String> readOverrides = new HashMap<>();
            readOverrides.put("database",client);

            ReadConfig readConfig = ReadConfig.create(sc).withOptions(readOverrides);

            JavaMongoRDD<Document> rdd = MongoSpark.load(sc,readConfig);
            BasicDBObject basicDBObject1 = new BasicDBObject();
            basicDBObject1.append("location_process", new BasicDBObject("$exists", true));


            JavaMongoRDD<Document> aggregatedRdd1 = rdd.withPipeline(
                    Arrays.asList(
                            new Document("$match", basicDBObject1)
                    ));

            aggregatedRdd1.foreachPartition(p -> {
                List<UpdateOneModel<Document>> myList = new ArrayList<>();
                MongoCollection<Document> mongoWriteCollection = MongoDB.getOutputCollection(broadcast_client.value(), "profileEvent");
                while (p.hasNext()) {
                    Document profile = p.next();
                    Object a = profile.get("location");
                    if (a != null && StringUtils.isNotBlank(a.toString()) && broadcast.value().contains(a.toString())) {
                        Object candidateId = profile.get("candidateId");
                        Document location_process =  profile.get("location_process",Document.class);

                        String StandardLocation = location_process.getString("location");
                        StandardLocation = StandardLocation.replaceAll(",", ", ");
                        Document document = new Document("event", "Cleaned_IM_Profile_Data")
                         .append("candidateId", candidateId.toString())
                         .append("refNum", profile.getString("refNum"))
                         .append("source", "CRM")
                         .append("timestamp", "12/12/2018 15:29:361")
                         .append("location", new BasicDBObject(StandardLocation, new BasicDBObject("city", a.toString())));
                        generateEvent(document.toJson(),propertiesBroadcast.value());

                        Document updateDocument = new Document("$set", new BasicDBObject("location_flag", 4));
                        myList.add(new UpdateOneModel(profile, updateDocument, new UpdateOptions().upsert(false)));
                    }

                    if(myList.size() >=1000 ||(myList.size()>0 && !p.hasNext())){
                        mongoWriteCollection.bulkWrite(myList);
                        myList.clear();
                    }
                }
            });

        }
    ElasticSearch.stopConnection();
    }

    public static void generateEvent(String string, Properties props) {
        Producer<String, String> producer = new KafkaProducer(props);
        ProducerRecord<String, String> record = new ProducerRecord(props.getProperty("crmoutputTopic"), string);
        producer.send(record);
        producer.close();

    }

}