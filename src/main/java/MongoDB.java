import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import config.Config;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bson.Document;
import scala.Serializable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MongoDB implements Serializable {

    static MongoClient mongoClient = new MongoClient(new MongoClientURI(Config.MONGODB_URI));
/*
    static {
        try {
            mongoClient = new MongoClient(new MongoClientURI(MainClass.loadConfigData("MongoClientURI")));
        } catch (IOException e) {
            System.out.println(" exception while opening db connection "+
            ExceptionUtils.getStackTrace(e));
        }
    }
*/


    public static MongoCollection<Document> getOutputCollection(String dbName, String collectionName){
        MongoDatabase database = mongoClient.getDatabase(dbName);
        MongoCollection<Document> collection = database.getCollection(collectionName);
        try {
            collection.createIndex(new Document("location", 1));
        }catch (Exception e){
            System.out.println("Index already there "+ ExceptionUtils.getStackTrace(e));
        }
        return collection;
    }

    public static DB readData(String database) throws IOException {
        DB db = mongoClient.getDB(database);
        return db;
    }


}