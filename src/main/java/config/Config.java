package config;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Properties;
public class Config {
    private static final Logger logger = LoggerFactory.getLogger(Config.class);
    private static String PROPERTIES_FILE = System.getProperty("propertyFile");
    public static Properties properties = new Properties();

    static {
        try {
            Configuration hdfsConf = new Configuration();
            FileSystem fs = FileSystem.get(hdfsConf);
            FSDataInputStream is = fs.open(new Path(PROPERTIES_FILE));
            properties.load(is);
            is.close();
        } catch (Exception e) {
            logger.error("exception while loading properites file " + ExceptionUtils.getFullStackTrace(e));
            System.out.println("exception while loading properites file " + ExceptionUtils.getFullStackTrace(e));
        }
    }


    public static final String MONGODB_URI = properties.getProperty("MONGODB_URI");
    public static final String KAFKA_BROKER_LIST = properties.getProperty("KAFKA_BROKER_LIST");
    public static final String STATE_CODES = properties.getProperty("STATE_CODES");
    public static final String PRODUCE_KAFKA_TOPIC_NAME = properties.getProperty("PRODUCE_KAFKA_TOPIC_NAME");

    public static final String KAFKA_VALUE_SERIALIZER = properties.getProperty("KAFKA_VALUE_SERIALIZER");
    public static final String KAFKA_KEY_SERIALIZER = properties.getProperty("KAFKA_KEY_SERIALIZER");

    public static final String REFNUMLIST = properties.getProperty("REFNUMLIST");

    public static final String ELASTICSEARCH_LOCATION_INDEX = properties.getProperty("ELASTICSEARCH_LOCATION_INDEX");
    public static final String ELASTICSEARCH_HOST = properties.getProperty("ELASTICSEARCH_HOST");
}
